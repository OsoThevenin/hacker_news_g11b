Rails.application.routes.draw do
  resources :contributions
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'contributions#index'
end
