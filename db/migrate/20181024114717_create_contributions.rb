class CreateContributions < ActiveRecord::Migration[5.2]
  def change
    create_table :contributions do |t|
      t.string :title
      t.string :url
      t.integer :points
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :contributions, [:user_id, :created_at]
  end
end
