# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!(name: "Carlos",
            email: "carlos@gmail.com")


Contribution.create!(title: "Example title",
                    url: "Example url",
                    points: 1234,
                    user: @user.first)

20.times do |n|
    title = Faker::Title.title
    url = "http://google.com/-#{n+1}-rails"
    points = %w{ 1 2 3 4 5 6 7 8 9 10 }
    user = @user.first
    Contribution.create!(title: title,
                        url: url,
                        points: points,
                        user: user)
end
