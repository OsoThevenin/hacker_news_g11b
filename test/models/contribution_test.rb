require 'test_helper'

class ContributionTest < ActiveSupport::TestCase
  def setup
    @user = users(:carlos)
    @contribution = @user.contributions.build(title: "Lorem ipsum", url: "Lorem ipsum", points: 0, user_id: @user.id)
  end

  test "should be valid" do
    assert @contribution.valid?
  end

  test "user id should be present" do
    @contribution.user_id = nil
    assert_not @contribution.valid?
  end

  test "title should be present" do
    @contribution.title = "   "
    assert_not @contribution.valid?
  end

  test "title should be at most 140 characters" do
    @contribution.title = "a" * 141
    assert_not @contribution.valid?
  end

  test "url should be present" do
    @contribution.url = "   "
    assert_not @contribution.valid?
  end

  test "order should be most recent first" do
    assert_equal contributions(:most_recent), Contribution.first
  end

end
