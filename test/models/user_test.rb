require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: "Example User", email: "user@example.com")
  end

  test "associated contributions should be destroyed" do
    @user.save
    @user.contributions.create!(title: "Lorem ipsum", url: "http://google.com", points: 4)
    assert_difference 'Contribution.count', -1 do
      @user.destroy
    end
  end
end
